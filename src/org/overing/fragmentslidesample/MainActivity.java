package org.overing.fragmentslidesample;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * 水平 Fragment 動話教學<br>
 * 因為3.0+ 的原生 Fragment 並不吃原來的 Animation 動畫效果<br>
 * 只能用新的 Animator Framework<br>
 * 又 Animator 並沒有內建 <b>"用百分比操作 View"</b> 的方式來執行動畫<br>
 * 查了查 StackOverflow 跟官方文件<br>
 * 其實要做的事情並不多 (簡單就是王道 :P)
 * <p>
 * * 在 /res/animator 裡建立動畫描述檔<br>
 * * 複寫一個 ViewGroup 來包裝 fragment view 代理水平滑動<br>
 * * fragment transaction 呼叫 setCustomAnimations 設定動話即可 
 * 
 * @author Overing
 * 
 */
public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		getFragmentManager()
				// 開始 fragment 交換儀式 :P
				.beginTransaction()
				// 第一張 fragment 進駐不加動畫
				.replace(R.id.layout_fragment_site, new FirstFragment())
				.commit(); // Go!
	}

	public static class FirstFragment extends Fragment //
			implements View.OnClickListener {

		@Override
		public View onCreateView(LayoutInflater i, ViewGroup c, Bundle s) {
			// 用有水平百分比設定功能的 FrameLayout 包覆 fragment 真正的 layout
			FrameLayout base = new FrameLayoutForSlide(getActivity());
			base.addView(i.inflate(R.layout.fragment_first, base, false));
			return base;
		}

		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
			super.onViewCreated(view, savedInstanceState);
			// 榜定按鈕事件
			view.findViewById(R.id.btn_slide_out).setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			if (v.getId() == R.id.btn_slide_out) {
				// 四個參數動畫參數指定 fragment 怎麼交換
				// 作用對向是指 "要刷入的新 fragment" (需要花點時間想像XD)
				// 這邊例子當然就是 SecondFragment (用 Second 替換 First)
				int enter = R.animator.right_slide_in; // 進入 layout 區塊時怎麼動
				int exit = R.animator.left_slide_out; // 離開 layout 區塊時怎麼動
				int popEnter = R.animator.left_slide_in; // 進入堆疊時怎麼動
				int popExit = R.animator.right_slide_out; // 從堆疊離開時怎麼動
				getFragmentManager()
						.beginTransaction()
						// 設定動話
						.setCustomAnimations(enter, exit, popEnter, popExit)
						.replace(R.id.layout_fragment_site,
								new SecondFragment()) //
						.addToBackStack(null) // 把它壓入堆疊 (這樣第二張要離開只要彈出堆疊就好)
						.commit(); // Go!
			}
		}
	}

	public static class SecondFragment extends Fragment //
			implements View.OnClickListener {

		@Override
		public View onCreateView(LayoutInflater i, ViewGroup c, Bundle s) {
			// 用有水平百分比設定功能的 FrameLayout 包覆 fragment 真正的 layout
			FrameLayout base = new FrameLayoutForSlide(getActivity());
			base.addView(i.inflate(R.layout.fregment_second, base, false));
			return base;
		}

		@Override
		public void onViewCreated(View view, Bundle savedInstanceState) {
			super.onViewCreated(view, savedInstanceState);
			// 榜定按鈕事件
			view.findViewById(R.id.btn_slide_out).setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			if (v.getId() == R.id.btn_slide_out) {
				// 把自己從 Stack 裡彈出, 這樣就會恢復 FirstFragment
				getFragmentManager().popBackStack();
			}
		}
	}

	/**
	 * 複寫 FrameLayout 增加 <b>"用百分比來操作 View 水平位置"</b> 的 方法(method)<br>
	 * 主要是要讓 /res/animator/* 裡的動畫效果檔作用<br>
	 * 沒包這層的話動話檔裡的 <br>
	 * <b>android:propertyName="xFraction"</b> </br> 是沒效果的!<br>
	 * <p>
	 * FrameLayout 的特性就是會讓子結點 View 自動占滿自己<br>
	 * 所以拿來用在這種包裝子結點 & 增加新功能<br>
	 * 但是又不汙染子結點程式碼時很好用
	 * 
	 * @author Overing
	 */
	public static class FrameLayoutForSlide extends FrameLayout {

		public FrameLayoutForSlide(Context context) {
			super(context);
		}

		/**
		 * 取得 layout 自身的 水平位置(X) 位置相對於寬度 (Width) 半分比
		 * 
		 * @return 水平位置百分比
		 */
		public float getXFraction() {
			return (getWidth() == 0) ? 0 : getX() / (float) getWidth();
		}

		/**
		 * 設定這個 layout 水平位置(用寬度的百分比)
		 * 
		 * @param xFraction
		 *            水平位置百分比
		 */
		public void setXFraction(float xFraction) {
			setX((getWidth() > 0) ? (xFraction * getWidth()) : 0);
		}
	}
}
